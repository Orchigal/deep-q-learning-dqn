import argparse
import os
os.environ["OMP_NUM_THREADS"] = "1"
import sys
import gym

import torch
import torch.optim as optim
import torch.multiprocessing as mp
import torch.nn as nn
import torch.nn.functional as F
from model import Actor
from train import train
from test import test
import my_optim
from rep_buff import replay_buffer

parser = argparse.ArgumentParser(description='DQN')
parser.add_argument('--lr', type=float, default=0.0001, metavar='LR',
                    help='learning rate (default: 0.0001)')
parser.add_argument('--gamma', type=float, default=0.99, metavar='G',
                    help='discount factor for rewards (default: 0.99)')
parser.add_argument('--seed', type=int, default=1, metavar='S',
                    help='random seed (default: 1)')
parser.add_argument('--num-processes', type=int, default=24, metavar='N',
                    help='how many training processes to use (default: 24)')
parser.add_argument('--buff-size', default=500000,
                    help='maximum buffer size')
parser.add_argument('--batch-size', default=32,
                    help='batch size for each training iteration')
parser.add_argument('--lr-freq', default=4,
                    help='learning frequency')
parser.add_argument('--lr-start', default=50000,
                    help='learning start after this number of iterations')
parser.add_argument('--target-update-freq', default = 10000,
                    help='update frequency of the target model')
parser.add_argument('--decay-coef', default = 1e6,
                    help='decay coefficient for action selection')
parser.add_argument('--prob-init', default = 0.1,
                    help='initial probability for selecting max action')
parser.add_argument('--prob-final', default = 1.0,
                    help='final probability for selecting max action')
parser.add_argument('--max-episode-length', type=int, default=10000, metavar='M',
                    help='maximum length of an episode (default: 10000)')
parser.add_argument('--env-name', default='PongDeterministic-v4', metavar='ENV',
                    help='environment to train on (default: PongDeterministic-v4)')
parser.add_argument('--no-shared', default=False, metavar='O',
                    help='use an optimizer without shared momentum.')


if __name__ == '__main__':
    os.environ['OMP_NUM_THREADS'] = '1'
  
    args = parser.parse_args()

    torch.manual_seed(args.seed)
    
    rep_buff = replay_buffer(args.buff_size)

    shared_model = Actor()
    shared_model.share_memory()

    if args.no_shared:
        optimizer = None
    else:
        optimizer = my_optim.SharedAdam(shared_model.parameters(), lr=args.lr)
        optimizer.share_memory()

    processes = []

    p = mp.Process(target=test, args=(args.num_processes, args, shared_model))
    p.start()
    processes.append(p)

    for rank in range(0, args.num_processes):
        p = mp.Process(target=train, args=(rank, args, shared_model, rep_buff, optimizer))
        p.start()
        processes.append(p)
    for p in processes:
        p.join()
