import math
import numpy as np  
import gym
import random

import torch
import torch.nn.functional as F
import torch.optim as optim
from model import Actor
from torch.autograd import Variable


def prepro(I):
    # preprocess the raw observation, turn into 80*80 array
    I = I[35:195] # crop
    I = I[::2,::2,0] # downsample by factor of 2
    I[I == 144] = 0 # erase background (background type 1)
    I[I == 109] = 0 # erase background (background type 2)
    I[I != 0] = 1 # everything else (paddles, ball) just set to 1
    I = I.reshape(1,1,80,80).astype('float32')
    I = torch.from_numpy(I)
    return I

def ensure_shared_grads(model, shared_model):
    for param, shared_param in zip(model.parameters(), shared_model.parameters()):
        if shared_param.grad is not None:
            return
        shared_param._grad = param.grad


def select_action(args, logit, t):
    fraction = min(t/args.decay_coef,1.0)
    rand_prob = args.prob_final + fraction * (args.prob_init - args.prob_final)
    sample = random.random()
    if sample > rand_prob:
        return logit.max(1)[1].data.numpy()[0]
    else:
        return random.randrange(4)
    
        
def train(rank, args, shared_model, rep_buff, optimizer=None):
    torch.manual_seed(args.seed + rank)

    env = gym.make(args.env_name)
    env.seed(args.seed + rank)

    model = Actor()
    model_old = Actor()

    if optimizer is None:
        optimizer = optim.Adam(shared_model.parameters(), lr=args.lr)

    model.train()
    model_old.eval()

    state = env.reset()
    state = prepro(state)
    done = True

    count = 0
    while True:
        count += 1
        model.load_state_dict(shared_model.state_dict())
        if done:
            cx = Variable(torch.zeros(1, 512))
            hx = Variable(torch.zeros(1, 512))
        else:
            cx = Variable(cx.data)
            hx = Variable(hx.data)

        cur_state = state.clone()
        cur_hx = hx.data.clone()
        cur_cx = cx.data.clone()
        logit, (hx,cx) = model((Variable(state),(hx,cx)))
        action = select_action(args, logit, count)
        state, reward, done, _ = env.step(action)
        state = prepro(state)
        
        rep_buff.insert(cur_state, cur_hx, cur_cx, action, reward, done, state.clone(), hx.data.clone(), cx.data.clone())
        
        if count%args.target_update_freq==0:
            model_old.load_state_dict(model.state_dict())
        
        if count%args.lr_freq==0 and count>=args.lr_start:
            states,hxs,cxs,actions,rewards,dones,next_states,next_hxs,next_cxs = rep_buff.sample(args.batch_size)
            
            logits, (_,_) = model((Variable(states),(Variable(hxs),Variable(cxs))))
            qs = logits.gather(1,Variable(actions))
            
            logits_old, (_,_) = model_old((Variable(next_states,volatile=True),
                                           (Variable(next_hxs,volatile=True),Variable(next_cxs,volatile=True))))
            qs_target = args.gamma*logits_old.data.max(1)[0].numpy()*(1-dones) + rewards
            qs_target = Variable(torch.from_numpy(qs_target.astype('float32')))
            
            loss = F.smooth_l1_loss(qs,qs_target)
            
            optimizer.zero_grad()
            loss.backward()
            for param in model.parameters():
                param.grad.data.clamp_(-1,1)
                
            ensure_shared_grads(model, shared_model)
            optimizer.step()
        
        if done:
            state = env.reset()
            state = prepro(state)
        
